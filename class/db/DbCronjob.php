<?php

/**
 * 
 * Description of DbCronjob
 *
 * @author Sander van Belleghem
 * @version 0.1
 * 
 */

require_once FILE_DB_DATABASE;

class DbCronjob extends Database {
    
    private $user_id;
    private $hunger_level;
    private $happiness_level;
    
    public function __construct() {
        parent::__construct();
    }
    
    /*
     * Getters
     * 
     * getUserId()
     *
     */    
    public function getUserId(){
        return $this->user_id;
    }    
    
    /*
     * getHungerLevel()
     *
     */    
    public function getHungerLevel(){
        return $this->hunger_level;
    }
    
    /*
     * getHappinessLevel()
     *
     */    
    public function getHappinessLevel(){
        return $this->happiness_level;
    }    
        
    /*
     * Setters
     * 
     * setUserId()
     *
     */    
    public function setUserId($user_id){
               
        if(!empty($user_id) && is_string($user_id)){
            $this->user_id = $user_id;
        }
    }    
    
    /*
     * setHungerLevel()
     *
     */    
    public function setHungerLevel($hunger_level){
        if(!empty($hunger_level) && is_int($hunger_level)){
            $this->hunger_level = $hunger_level;
        }
    }
    
    /*
     * setHappinessLevel()
     *
     */    
    public function setHappinessLevel($happiness_level){
        if(!empty($happiness_level) && is_int($happiness_level)){
            $this->happiness_level = $happiness_level;
        }
    } 
    
    /*
     * getUserStats()
     *
     */
    public function getUserStats(){
        
        $this->dbquery("SELECT * FROM `facebook_baby`.`stats`");
        
        // If there is a result.
        if($this->dbNumRows() >= 1){
            // Return array.
            return $this->dbFetchAll();        
        }
    }
        
    /*
     * update()
     * 
     */
    public function update(){
        
        $dbquery = ("UPDATE `facebook_baby`.`stats` 
                        SET happiness_level = '" . mysql_real_escape_string($this->happiness_level) . "',
                            hunger_level = '" . mysql_real_escape_string($this->hunger_level) . "'
                        WHERE `stats`.`user_id` ='" . mysql_real_escape_string($this->user_id) . "'");
        
        // If query returns false.
        if(!$this->dbquery($dbquery)){
            return FALSE;
        }
        
        return TRUE;
    }
}

?>
