<?php

/**
 * 
 * Description of Cronjob
 *
 * @author Sander van Belleghem
 * @version 0.1
 * 
 */

require_once FILE_CLASS_DATE_TIME;
require_once FILE_DB_CRONJOB;
require_once FILE_INCLUDES_FACEBOOK_SDK;

class Cronjob {

    private $db_cronjob;
    private $facebook;
    private $appId;
    private $secret;
    private $user_id;
    private $hunger_level;
    private $happiness_level;
    
    public function __construct() {

        // Make instance from DbCronjob for handeling database actions.
        $this->db_cronjob = new DbCronjob();

        // Initialize variables.
        $this->appId = FACEBOOK_APP_ID;
        $this->secret = FACEBOOK_SECRET;
        
        // Make instance from Facebook class for Facebook integration.
        $this->facebook = new Facebook( array (
            'appId'  => $this->appId,
            'secret' => $this->secret,
        ));
        
        // Get all data from all excisting users.
        $this->getUserStats();
    }

    /*
     * getUserStats()
     *
     */
    public function getUserStats() {
        // Request all data from database.
        $data = $this->db_cronjob->getUserStats();

        // If data returned.
        if (!empty($data)) {
            $this->checkLevels($data);
        }
    }

    /*
     * checkLevels()
     *
     */
    public function checkLevels($data) {

        foreach ($data as $userdata) {
            
            // Initialize variables.
            $hours = 0;
            $minutes = 0;
            $total_time = '';
            $this->user_id = $userdata['user_id'];
            
            // Calculate difference between saved date time and current date time.
            $datetime = dateDiff(date("Y-m-d H:i:s"), $userdata['datetime']);

            // If difference between saved date time and current date time is bigger than one year.
            if (!empty($datetime['years'])) {
                // Calculate total hours by multiplying hours per year with calculates difference.
                $total_time = (HOURS_PER_YEAR * $datetime['years']);
            }

            // If difference between saved date time and current date time is bigger than one month.
            if (!empty($datetime['months'])) {
                // Calculate total hours by multiplying hours per month with calculates difference.
                $total_time += HOURS_PER_MONTH * $datetime['months'];
            }

            // If difference between saved date time and current date time is bigger than one day.
            if (!empty($datetime['days'])) {  
                // Calculate total hours by multiplying hours per day with calculates difference.
                $total_time += HOURS_PER_DAY * $datetime['days'];
            }

            // If difference between saved date time and current date time is bigger than one second.
            if (!empty($datetime['seconds'])) {
                // If the difference equals 59 seconds, add one minute.
                if($datetime['seconds'] == 59){                    
                    $minutes += 1;
                }
            }

            // If difference between saved date time and current date time is bigger than one minute.
            if (!empty($datetime['minutes'])) {  
                // Calculate the total amount of minutes.
                $minutes = $minutes += $datetime['minutes'];
                // If the difference equals 59 minutes, add one hour.
                if($minutes == 59){
                   $hours += 1;
                }
            }
            
            // If difference between saved date time and current date time is bigger than one hour.
            if (!empty($datetime['hours'])) {
                // Calculate the total amount of hours.
                $hours = $hours += $datetime['hours'];
                // Add the total amount of hours to the total time.
                $total_time += $hours;
            }
           
            // If Sleep mode is disabled.
            if($userdata['sleep_mode'] == 0){
                
                // Calculate new Hunger Level based on passed time.           
                $this->hunger_level = ($userdata['hunger_level'] - ($total_time * 15));

                // If Hunger Level is lower or equals than 1, set value to 1.
                if($this->hunger_level <= 1){
                    $this->hunger_level = 1;
                }            

                // Calculate new Happiness Level based on passed time.
                $this->happiness_level = ($userdata['happiness_level'] - ($total_time * 15));

                // If Happiness Level is lower or equals than 1, set value to 1.
                if($this->happiness_level <= 1){
                    $this->happiness_level = 1;
                }
            // Sleep mode is enabled.
            } else {
                // Calculate new Hunger Level based on passed time.           
                $this->hunger_level = ($userdata['hunger_level'] + ($total_time * 15));

                // If Hunger Level is higher or equals 100, set value to 100.
                if($this->hunger_level >= 100){
                    $this->hunger_level = 100;
                }            

                // Calculate new Happiness Level based on passed time.
                $this->happiness_level = ($userdata['happiness_level'] + ($total_time * 15));

                // If Happiness Level is higher or equals 100, set value to 100.
                if($this->happiness_level >= 100){
                    $this->happiness_level = 100;
                }
            }
            
            if($this->happiness_level < 50){
                // Don't send a notification in the middle of the night.
                if((date('H') >= 9) && (date('H') <= 21)){
                    // Set Facebook Notification message.
                    $message = "I need some Attention!";

                    // Send Facebook Notification.
                    $this->facebookNotification($message);    
                }
            } elseif($this->hunger_level < 50){
                // Don't send a notification in the middle of the night.
                if((date('H') >= 9) && (date('H') <= 21)){
                    // Set Facebook Notification message.
                    $message = "I'm Hungry!";

                    // Send Facebook Notification.
                    $this->facebookNotification($message);    
                }
            }            
            
            // If update query returns true, reset class attributes.
            if($this->update()){
                $this->reset();
            }
        }
    }
    
    /*
     * facebookNotification()
     * 
     */
    private function facebookNotification($message) {

        $accessToken = $this->appId . '|' . $this->secret;
        
        $canvasPage = 'http://facebook.tim-online.nl/babyhuis/';
        
        $params = array(
            'access_token' => $accessToken,
            'href' => $canvasPage,
            'template' => $message,
        );

        $this->facebook->api('/' . $this->userId . '/notifications/', 'post', $params);                
    }
    
    /*
     * update()
     * 
     */
    public function update() {
        $this->db_cronjob->setUserId($this->user_id);
        $this->db_cronjob->setHungerLevel($this->hunger_level);
        $this->db_cronjob->setHappinessLevel($this->happiness_level);
        
        if($this->db_cronjob->update()){
            return TRUE;
        }
    }
    
    /*
     * reset()
     * 
     */
    public function reset() {        
        $this->user_id = '';
        $this->hunger_level = '';
        $this->happiness_level = '';
    }    
}

?>