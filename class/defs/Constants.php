<?php

/**
 * 
 * Description of file constants
 * 
 * @version 0.1
 * @author Sander van Belleghem
 * 
 */

set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT'] . '/');

// Root
define('WWW_ROOT',                          '');

define('HOME_PAGE',                         WWW_ROOT . 'index.php');

// Directories
define('DIR_CLASS',                         WWW_ROOT . 'class/');  
define('DIR_DB',                            DIR_CLASS . 'db/');
define('DIR_DEFS',                          DIR_CLASS . 'defs/');
define('DIR_DEFS',                          DIR_CLASS . 'includes/');

// Includes
define('FILE_INCLUDES_FACEBOOK_SDK',        DIR_INCLUDES . 'facebook-php-sdk/src/facebook.php');

// Class Files
define('FILE_CLASS_CRONJOB',                DIR_CLASS . 'Cronjob.php');
define('FILE_CLASS_DATE_TIME',              DIR_CLASS . 'DateTime.php');

// Database Files
define('FILE_DB_CONSTANTS',                 DIR_DB . 'DbConstants.php');
define('FILE_DB_CRONJOB',                   DIR_DB . 'DbCronjob.php');
define('FILE_DB_DATABASE',                  DIR_DB . 'Database.php');
define('FILE_DB_ERROR',                     DIR_DB . 'Error.php');

// Constants
define('HOURS_PER_YEAR',                    '8760');
define('HOURS_PER_MONTH',                   '730.5');
define('HOURS_PER_DAY',                     '24');

// Facebook Constants
define('FACEBOOK_APP_ID',                   '534629336589338');
define('FACEBOOK_SECRET',                   '2e08fad0227d20bb7b4ab0d7db3e18b1');

?>